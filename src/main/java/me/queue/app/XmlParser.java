package me.queue.app;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;

public class XmlParser {
    public static Object XmlStringToObject(Class convertedObject, String xmlString) {
        if(xmlString==null)return null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(convertedObject);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            StringReader reader = new StringReader(xmlString);
            return unmarshaller.unmarshal(reader);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String ObjectToXmlString(@SuppressWarnings("rawtypes") Class convertedObjectClass, Object convertedObject) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(convertedObjectClass);
            Marshaller marshaller = jaxbContext.createMarshaller();

            StringWriter writer = new StringWriter();
            marshaller.marshal(convertedObject, writer);

            Integer i=writer.toString().indexOf(">")+1;

            return writer.toString().substring(i);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return "";
    }
}

