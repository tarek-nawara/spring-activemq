package me.queue.app;

import com.ibm.mq.jms.MQDestination;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.jms.Destination;
import javax.jms.JMSException;

@Component
public class QueueFactory {

    @Value("${client.mode.production:false}")
    Boolean productionMode;

    Destination createDestination(String destinationName) throws JMSException {
        return !productionMode? new ActiveMQQueue(destinationName): new MQDestination(destinationName);
    }
}
