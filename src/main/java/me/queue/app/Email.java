package me.queue.app;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Email")
@XmlAccessorType(XmlAccessType.FIELD)
public class Email {
    @XmlElement(name = "Name")
    private String name;

    @XmlElement(name = "Password")
    private String password;

    public Email() {
    }

    public Email(final String name, final String password){
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Email{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
