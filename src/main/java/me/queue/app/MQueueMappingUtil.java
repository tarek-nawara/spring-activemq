package me.queue.app;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MQueueMappingUtil {
    @Value("${mq.listener.destination.queue}")
    public String testingQueueRq;

    @Value("${mq.listener.replyTo.queue}")
    public String testingQueueRs;
}
