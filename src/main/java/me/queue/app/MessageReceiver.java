package me.queue.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibm.mq.jms.MQDestination;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.*;
import java.util.function.Consumer;

@Component
public class MessageReceiver {

    @Value("${mq.listener.source.queue:testqueue}")
    String queueName;

    @Value("${mq.listener.dest.queue:responsequeue}")
    String replyQueueName;

    @Autowired
    JmsTemplate jmsTemplate;

    @Autowired
    QueueFactory factory;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    ConnectionFactory connectionFactory;

    public Email createEmail(final String username, final String password) throws Exception {
        jmsTemplate.convertAndSend(queueName, new Email(username, password));

        Connection connection = connectionFactory.createConnection();
        Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
        connection.start();

        MessageConsumer consumer = session.createConsumer(factory.createDestination(queueName));

        ActiveMQTextMessage message = (ActiveMQTextMessage) consumer.receive();
        System.out.println("In Message Receiver, message=" + message);
        connection.close();
        return objectMapper.readValue(message.getText(), Email.class);
    }
}
