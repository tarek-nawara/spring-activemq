package me.queue.app;

import com.ibm.mq.jms.MQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.UUID;

@Service
public class JmsMessagingConnector {
    private static final int READ_SIZE = 256;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private Environment env;

    @PostConstruct
    public void init() {
        jmsTemplate.setSessionAcknowledgeMode(Session.CLIENT_ACKNOWLEDGE);
    }

    public String sendSyncByteMessage(final String serviceName,
                                      final String message) throws Exception {
        final String destinationQueueName = env.getProperty(serviceName + ".destination");
        final String replyToQueueName = env.getProperty(serviceName + ".destination");
        jmsTemplate.send(destinationQueueName, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                BytesMessage bytesMessage = session.createBytesMessage();
                bytesMessage.writeBytes(message.getBytes());
                bytesMessage.setJMSReplyTo(new MQQueue(replyToQueueName));
                return bytesMessage;
            }
        });
        String response = receiveMessage(replyToQueueName, null);
        System.out.println("In JmsMessagingConnector send async, received message=" + response);
        return response;
    }


    public String sendSyncTextMessage(final String serviceName,
                                       final String message) throws Exception {
        final String destinationQueueName = env.getProperty(serviceName + ".destination");
        final String replyToQueueName = env.getProperty(serviceName + ".destination");
        jmsTemplate.send(destinationQueueName, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                TextMessage textMessage = session.createTextMessage();
                textMessage.setText(message);
                textMessage.setJMSReplyTo(new MQQueue(replyToQueueName));
                return textMessage;
            }
        });
        String response = receiveMessage(replyToQueueName, null);
        System.out.println("In JmsMessagingConnector send async, received message=" + response);
        return response;
    }

    public String sendASyncByteMessage(final String serviceName,
                                       final String message) throws Exception {
        final String destinationQueueName = env.getProperty(serviceName + ".destination");
        final String replyToQueueName = env.getProperty(serviceName + ".destination");
        final String correlationId = UUID.randomUUID().toString();
        jmsTemplate.send(destinationQueueName, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                BytesMessage bytesMessage = session.createBytesMessage();
                bytesMessage.writeBytes(message.getBytes());
                bytesMessage.setJMSCorrelationID(correlationId);
                bytesMessage.setJMSReplyTo(new MQQueue(replyToQueueName));
                return bytesMessage;
            }
        });
        String response = receiveMessage(replyToQueueName, correlationId);
        System.out.println("In JmsMessagingConnector send async, received message=" + response);
        return response;
    }

    public String sendASyncTextMessage(final String serviceName,
                                       final String message) throws Exception {
        final String destinationQueueName = env.getProperty(serviceName + ".destination");
        final String replyToQueueName = env.getProperty(serviceName + ".destination");
        final String correlationId = UUID.randomUUID().toString();
        jmsTemplate.send(destinationQueueName, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                TextMessage textMessage = session.createTextMessage();
                textMessage.setText(message);
                textMessage.setJMSCorrelationID(correlationId);
                textMessage.setJMSReplyTo(new MQQueue(replyToQueueName));
                return textMessage;
            }
        });
        String response = receiveMessage(replyToQueueName, correlationId);
        System.out.println("In JmsMessagingConnector send async, received message=" + response);
        return response;
    }


    public String receiveMessage(String replyToQueueName, String correlationId) throws JMSException, IOException {
        Message receivedMessage;
        if (correlationId != null) {
            receivedMessage =
                    jmsTemplate.receiveSelected(replyToQueueName, String.format("JMSCorrelationID = '%s'", correlationId));
        } else {
            receivedMessage = jmsTemplate.receive(replyToQueueName);
        }
        if (receivedMessage == null) {
            throw new RuntimeException("Received null message");
        }
        if (receivedMessage instanceof BytesMessage) {
            BytesMessage receivedByteMessage = (BytesMessage) receivedMessage;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[READ_SIZE];
            while (receivedByteMessage.readBytes(buf) > 0) {
                out.write(buf);
            }
            return out.toString().trim();
        } else if (receivedMessage instanceof TextMessage) {
            TextMessage receivedTextMessage = (TextMessage) receivedMessage;
            return receivedTextMessage.getText();
        } else {
            throw new RuntimeException("Received unsupported message, message=" + receivedMessage);
        }
    }

}
