package me.queue.app.controller;

import me.queue.app.Email;
import me.queue.app.MessageReceiver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QueueController {

    @Autowired
    private MessageReceiver messageReceiver;

    @GetMapping("/register")
    public Email register() throws Exception {
        return messageReceiver.createEmail("testusername", "testpassword");
    }
}
