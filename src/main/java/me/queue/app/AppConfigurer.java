package me.queue.app;

import com.ibm.mq.jms.MQConnectionFactory;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MarshallingMessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import javax.jms.ConnectionFactory;

@Configuration
public class AppConfigurer {

    @Value("${client.mode.production:false}")
    Boolean productionMode;

    @Bean
    public JmsListenerContainerFactory<?> appFactory(ConnectionFactory connectionFactory,
                                                    DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        // This provides all boot's default to this factory, including the message converter
        configurer.configure(factory, connectionFactory);
        // You could still override some of Boot's default if necessary.
        return factory;
    }

//    @Bean // Serialize message content to json using TextMessage
//    public MessageConverter jaxb2JmsMessageConverter() {
//        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
//        converter.setTargetType(MessageType.TEXT);
//        converter.setTypeIdPropertyName("_type");
//        return converter;
//        MarshallingMessageConverter converter = new MarshallingMessageConverter();
//        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
//        marshaller.setPackagesToScan("me.queue.app");
//        converter.setMarshaller(marshaller);
//        converter.setUnmarshaller(marshaller);
//        return converter;
//    }


    @Bean
    public ConnectionFactory connectionFactory(){
//        ConnectionFactory connectionFactory = !productionMode? new ActiveMQConnectionFactory(): new MQQueueConnectionFactory();
        ConnectionFactory connectionFactory = new MQQueueConnectionFactory();
        return connectionFactory;
    }

}
